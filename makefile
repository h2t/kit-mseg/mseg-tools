# Internal variables
deb_dist = ./deb-dist

.depsdeb:
	@echo python
	@echo python-argcomplete
	@echo python-git
	@echo python-setuptools
	@echo python-stdeb

.bashrc:
	@printf "# <MSEG-TOOLS>\n"
	@printf "export MSEG_TOOLS_DIR=\"$(realpath $(dir $(lastword $(MAKEFILE_LIST))))\"\n"
	@printf "# Add local bin folder to path (for Python setuptools executables)\n"
	@printf "export PATH=\"\$${PATH}:\$${HOME}/.local/bin\"\n"
	@printf "# Register autocomplete\n"
	@printf "eval \"\$$(register-python-argcomplete msegcm)\"\n"
	@printf "eval \"\$$(register-python-argcomplete msegdata)\"\n"
	@printf "eval \"\$$(register-python-argcomplete mseggen)\"\n"
	@printf "# </MSEG-TOOLS>\n\n"

clean:
	@rm -r ./build 2> /dev/null || true
	@rm -r ./dist 2> /dev/null || true
	@rm -r ${deb_dist} 2> /dev/null || true
	@rm -r ./deb_dist 2> /dev/null || true
	@rm -r ./mseg_tools.egg-info 2> /dev/null || true
	@# Deletes all installed files outside of the repository
	@xargs rm 2> /dev/null < .installed-files.log || true
	@rm .installed-files.log 2> /dev/null || true

install:
	@# installed files will be written into `.installed-files.log`
	python setup.py install --user --record .installed-files.log

package:
	@mkdir ${deb_dist} 2> /dev/null || true
	python setup.py --command-packages=stdeb.command bdist_deb
	@mv ./deb_dist/*.deb ${deb_dist}/
