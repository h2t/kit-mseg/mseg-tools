"""
This file is part of ArmarX.

ArmarX is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

ArmarX is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

@package    MSeg::mseg
@author     Christian R. G. Dreher ( cdreher3011 at gmail dot com )
@date       2017
@copyright  http://www.gnu.org/licenses/gpl-2.0.txt
            GNU General Public License
"""

import os
import sys

class FileMapping:
    def __init__(self, source_path, dest_path, executable = False):
        self.source_path = source_path
        self.dest_path = dest_path
        self.executable = executable

        with open(self.source_path) as f:
            self.file_contents = f.readlines()

    def write_to_dest(self):
        print 'Installing ' + self.dest_path

        file = open(self.dest_path, 'w')

        for x in self.file_contents:
            file.write(x)

        file.close()

        if self.executable:
            os.chmod(self.dest_path, 0744)


def run(args):
    try:
        name = _format_name(args.name)
    except:
        sys.stderr.write('Error: Project name must be alphanumeric and start with a character\n')
        sys.stderr.flush()
        sys.exit(2)
    template_root_dir = ''
    mseg_tools_dir = os.environ.get('MSEG_TOOLS_DIR', None)

    if mseg_tools_dir is None or mseg_tools_dir == '':
        template_root_dir = '/usr/share/mseg-tools/templates'
    else:
        template_root_dir = os.path.join(mseg_tools_dir, 'templates')

    pli = 'cpp' if args.cpp else ('python' if args.python else ('java' if args.java else ('matlab' if args.matlab else '')))
    tmpl_path = os.path.join(template_root_dir, 'algorithm-skeleton-' + pli)
    dest_path = args.base_path
    project_dir = os.path.join(dest_path, name.lower())
    train = args.train

    variables = []
    variables.append(('@PROJECT_NAME@', name))
    variables.append(('@PROJECT_NAME_LOWER@', name.lower()))
    variables.append(('@PROJECT_NAME_UPPER@', name.upper()))
    variables.append(('@IF_TRAIN@', '' if train else get_comment_for_language(pli)))

    files = get_files(pli, tmpl_path, project_dir, name)

    # Replace placeholder
    files = replace_variables(files, variables)

    # Write to disk
    try:
        create_directories(pli, project_dir)
    except:
        sys.stderr.write('Error: The file or directory `{}` already exists\n'.format(project_dir))
        sys.stderr.flush()
        sys.exit(1)
    for file in files:
        file.write_to_dest()


def get_files(pli, tmpl_path, project_dir, name):
    files = []

    if pli == 'cpp':

        files.append(FileMapping(
            os.path.join(tmpl_path, 'CMakeLists.txt.tmpl'),
            os.path.join(project_dir, 'CMakeLists.txt')))

        files.append(FileMapping(
            os.path.join(tmpl_path, 'main.cpp.tmpl'),
            os.path.join(project_dir, 'source', 'main.cpp')))

        files.append(FileMapping(
            os.path.join(tmpl_path, 'algorithm.cpp.tmpl'),
            os.path.join(project_dir, 'source', name.lower() + '.cpp')))

        files.append(FileMapping(
            os.path.join(tmpl_path, 'algorithm.h.tmpl'),
            os.path.join(project_dir, 'source', name.lower() + '.h')))

    elif pli == 'python':

        files.append(FileMapping(
            os.path.join(tmpl_path, 'algorithm.py.tmpl'),
            os.path.join(project_dir, name.lower() + '.py')))

    elif pli == 'java':

        files.append(FileMapping(
            os.path.join(tmpl_path, 'build.xml.tmpl'),
            os.path.join(project_dir, 'build.xml')))
        
        files.append(FileMapping(
            os.path.join(tmpl_path, 'algorithm.java.tmpl'),
            os.path.join(project_dir, 'src', name + '.java')))

    elif pli == 'matlab':

        files.append(FileMapping(
            os.path.join(tmpl_path, 'main.tmpl'),
            os.path.join(project_dir, 'main'),
            True))

        files.append(FileMapping(
            os.path.join(tmpl_path, 'setup.m.tmpl'),
            os.path.join(project_dir, 'setup.m')))

        files.append(FileMapping(
            os.path.join(tmpl_path, 'constructor.m.tmpl'),
            os.path.join(project_dir, 'algorithm', 'constructor.m')))

        files.append(FileMapping(
            os.path.join(tmpl_path, 'reset_training.m.tmpl'),
            os.path.join(project_dir, 'algorithm', 'reset_training.m')))

        files.append(FileMapping(
            os.path.join(tmpl_path, 'segment.m.tmpl'),
            os.path.join(project_dir, 'algorithm', 'segment.m')))

        files.append(FileMapping(
            os.path.join(tmpl_path, 'train.m.tmpl'),
            os.path.join(project_dir, 'algorithm', 'train.m')))

    return files


def create_directories(pli, project_dir):
    print 'Creating project directory "' + project_dir + '"'

    os.makedirs(project_dir)

    if pli == 'cpp':

        print 'Creating source code and build directory'

        os.makedirs(os.path.join(project_dir, 'build'))
        os.makedirs(os.path.join(project_dir, 'source'))

    elif pli == 'java':

        print 'Creating source code, build and lib directory'

        os.makedirs(os.path.join(project_dir, 'build'))
        os.makedirs(os.path.join(project_dir, 'src'))
        os.makedirs(os.path.join(project_dir, 'lib'))

    elif pli == 'matlab':

        print 'Creating algorithm source directory'

        os.makedirs(os.path.join(project_dir, 'algorithm'))


def get_comment_for_language(language):
    if language == 'python':
        return '# '
    elif language == 'matlab':
        return '% '
    else:
        return '// '


def replace_variables(files, variables):
    for file in files:
        for search, replace in variables:
            file.file_contents = [x.replace(search, replace) for x in file.file_contents]

    return files


def _format_name(name):
    cleared_name = ''.join([c for c in name if c.isalnum()])

    try:
        while not cleared_name[0].isalpha():
            cleared_name = cleared_name[1:]
    except:
        cleared_name = ''

    if cleared_name == '':
        raise Exception('Invalid name')

    return cleared_name
