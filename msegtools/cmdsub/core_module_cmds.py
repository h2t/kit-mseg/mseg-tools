"""
This file is part of ArmarX.

ArmarX is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

ArmarX is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

@package    MSeg::mseg
@author     Christian R. G. Dreher ( cdreher3011 at gmail dot com )
@date       2017
@copyright  http://www.gnu.org/licenses/gpl-2.0.txt
            GNU General Public License
"""

import os, errno, signal, subprocess, time
from datetime import datetime
from ctypes import cdll, c_char_p

from msegtools.utility import get_mseg_home_path


PIDPATH = os.path.join(get_mseg_home_path(os.path.expanduser('~/.mseg')), 'processes')

DATA_EX_PATH = 'DataExchangeAppRun'
SEG_CON_PATH = 'SegmentationControllerAppRun'
EVA_CON_PATH = 'EvaluationControllerAppRun'

DATA_EX_NAME = 'dataexchange'
SEG_CON_NAME = 'segmentationcontroller'
EVA_CON_NAME = 'evaluationcontroller'

DEVNULL = open(os.devnull, 'r+b')


def start(args=None):
    process_start(DATA_EX_NAME, DATA_EX_PATH)
    process_start(SEG_CON_NAME, SEG_CON_PATH)
    process_start(EVA_CON_NAME, EVA_CON_PATH)


def stop(args=None):
    process_stop(DATA_EX_NAME, DATA_EX_PATH)
    process_stop(SEG_CON_NAME, SEG_CON_PATH)
    process_stop(EVA_CON_NAME, EVA_CON_PATH)


def status(args=None):
    dex_status, dex_pid = process_status(DATA_EX_NAME, DATA_EX_PATH)
    seg_con_status, seg_con_pid = process_status(SEG_CON_NAME, SEG_CON_PATH)
    eva_con_status, eva_con_pid = process_status(EVA_CON_NAME, EVA_CON_PATH)

    if dex_status == seg_con_status and dex_status == eva_con_status:
        cm_status = '[' + dex_status + ']'
    else:
        cm_status = '  [mixed]'

    print cm_status + ' MSeg core module'

    #print '[running] M'
    print '          |--- [' + dex_status + '] DataExchange' + ('' if dex_pid < 0 else ' (PID: ' + str(dex_pid) + ')')
    print '          |--- [' + seg_con_status + '] SegmentationController' + ('' if seg_con_pid < 0 else ' (PID: ' + str(seg_con_pid) + ')')
    print '          `--- [' + eva_con_status + '] EvaluationController' + ('' if eva_con_pid < 0 else ' (PID: ' + str(eva_con_pid) + ')')


def restart(args=None):
    stop()
    start()


def process_start(name, path):
    # Check if the process is already running
    status, pid = process_status(name, path)

    if status == 'running':
        return

    # Start process
    process = subprocess.Popen([path], stdin=DEVNULL, stdout=DEVNULL, stderr=DEVNULL)

    try:
        os.makedirs(PIDPATH)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

    # Write PID file
    pidfilename = os.path.join(PIDPATH, name + '.pid')
    pidfile = open(pidfilename, 'w')
    pidfile.write(str(process.pid))
    pidfile.close()


def process_stop(name, path):
    status, pid = process_status(name, path)

    if status == 'stopped':
        return

    os.kill(pid, signal.SIGTERM)

    starttime = datetime.now()

    # Wait for the process to have actually stopped
    while pid > 0:
        status, pid = process_status(name, path)

        waittime = datetime.now() - starttime

        # If the process did not stop after 3 seconds, send SIGKILL
        if waittime.total_seconds() >= 3:
            os.kill(pid, signal.SIGKILL)
            starttime = datetime.now()


def process_status(name, path):
    status = 'stopped'
    pid = -1

    pidfilename = os.path.join(PIDPATH, name + '.pid')

    # Try to open pidfile. If not exists: Not running
    try:
        pidfile = open(pidfilename, 'r')
        pid = int(pidfile.read())
    except IOError as e:
        if e.errno == errno.ENOENT:
            return status, pid

    # Check if process with given PID is actually running
    if pid != -1 and os.path.exists(os.path.join('/proc', str(pid))):
        status = 'running'
    else:
        # Clean up if there was a PID file but the process doesn't exist
        os.remove(os.path.join(PIDPATH, name + '.pid'))

    return status, pid
