"""
This file is part of ArmarX.

ArmarX is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

ArmarX is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

@package    MSeg::mseg
@author     Christian R. G. Dreher ( christian.dreher@student.kit.edu )
@date       2018
@copyright  http://www.gnu.org/licenses/gpl-2.0.txt
            GNU General Public License
"""

import os
import sys
from git import Repo, RemoteProgress

from msegtools.utility import get_motion_data_path


MSEG_MOTION_DATA_REPO_URL = 'https://gitlab.com/h2t/kit-mseg/datasets.git'


class ProgressPrinter(RemoteProgress):
    def update(self, op_code, cur_count, max_count=None, message=''):
        if op_code & RemoteProgress.COUNTING:
            sys.stdout.write('Preparing... ')
        if op_code & RemoteProgress.COMPRESSING:
            sys.stdout.write('Compressing on server... ')
        if op_code & RemoteProgress.WRITING:
            sys.stdout.write('Writing... ')
        if op_code & RemoteProgress.RECEIVING:
            sys.stdout.write('Downloading... ')
        if op_code & RemoteProgress.RESOLVING:
            sys.stdout.write('Extracting... ')
        if op_code & RemoteProgress.END:
            sys.stdout.write('done   ') # Trailing whitespaces to overwrite previous outputs
            sys.stdout.flush()
            print
            return
        percentage = (float(cur_count) / (float(max_count) or 100.0) * 100)
        percentage_str = '{0:.2f}%  '.format(percentage) # Trailing whitespaces to overwrite previous outputs
        #print percentage
        sys.stdout.write('{}\r'.format(percentage_str))
        sys.stdout.flush()


def fetch(args):
    print 'Fetching dataset...'
    repo_path = get_motion_data_path(os.path.expanduser('~/.mseg/motion-data'))
    Repo.clone_from(MSEG_MOTION_DATA_REPO_URL, repo_path, progress=ProgressPrinter())
    print 'The dataset was successfully extracted to `{}`'.format(repo_path)


def update(args):
    print 'Updating dataset...'
    repo_path = get_motion_data_path(os.path.expanduser('~/.mseg/motion-data'))
    repo = Repo(repo_path)
    fetch_info = repo.remotes.origin.pull(progress=ProgressPrinter())[0]
    if fetch_info.old_commit is None:
        print 'Dataset is up-to-date'
