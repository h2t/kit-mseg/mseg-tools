"""
This file is part of ArmarX.

ArmarX is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

ArmarX is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

@package    MSeg::scripts
@author     Nicklas Kulp ( nicklas-kulp at gmx dot de )
@date       2017
@copyright  http://www.gnu.org/licenses/gpl-2.0.txt
            GNU General Public License
"""

import fnmatch
import os
from os.path import expanduser

def license(args):
    mseg_dir = os.environ.get('MSEG_DIR', None)

    if mseg_dir is None:
        print 'Could not find base directory'
        return

    matches = []
    for root, dirnames, filenames in os.walk(mseg_dir):
        for filename in filenames:
            if not (
              '/build' in root or 
              '/.git' in root or 
              '/tools' in root or 
              '/documentation' in root or 
              '/data' in root or 
              '/motion-data' in root or 
              '/scenarios' in root or 
              '/doc/' in root or 
              'cmake' in filename or 
              'CMakeLists' in filename or 
              '.png' in filename or 
              '.svg' in filename or 
              '.dox' in filename or 
              '.qrc' in filename or 
              '.gitignore' in filename or 
              'LICENSE' in filename or 
              'README' in filename or 
              'gpl-2.0' in filename or 
              '.ui' in filename or 
              '.md' in filename or 
              '.pyc' in filename or 
              '.html' in filename or 
              '.xml' in filename or 
              '.css' in filename or 
              '.yml' in filename or 
              '.tmpl' in filename or 
              'Gemfile' in filename
            ):
                matches.append(os.path.join(root, filename))
            
    for match in matches:
        file = open(match, 'r')
        if not 'This file is part of ArmarX.' in file.read(200):
            print match

