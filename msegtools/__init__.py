"""
This file is part of ArmarX.

ArmarX is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

ArmarX is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

@package    MSeg::mseg
@author     Christian R. G. Dreher ( cdreher3011 at gmail dot com )
@date       2017
@copyright  http://www.gnu.org/licenses/gpl-2.0.txt
            GNU General Public License
"""

import argcomplete, argparse
import sys
import os

from msegtools.cmdsub import core_module_cmds
from msegtools.cmdsub import generate_cmds
from msegtools.cmdsub import dataset_cmds
from msegtools.cmdsub import dev_cmds
from msegtools.utility import get_version

import __version__ as v


def msegcm_main():
    parser = argparse.ArgumentParser(prog="msegcm", description='MSeg core module management')
    subparsers = parser.add_subparsers(help='')

    # -v --version flags
    parser.add_argument('-v', '--version', action=VersionAction, nargs=0, help='Display relevant version information')

    # start command
    parser_start = subparsers.add_parser('start', help='Start the core module')
    parser_start.set_defaults(func=core_module_cmds.start)

    # stop command
    parser_stop = subparsers.add_parser('stop', help='Stop the core module')
    parser_stop.set_defaults(func=core_module_cmds.stop)

    # restart command
    parser_restart = subparsers.add_parser('restart', help='Restart the core module')
    parser_restart.set_defaults(func=core_module_cmds.restart)

    # status command
    parser_status = subparsers.add_parser('status', help='Status of the core module')
    parser_status.set_defaults(func=core_module_cmds.status)

    _parse_args(parser)


def msegdata_main():
    parser = argparse.ArgumentParser(prog='msegdata', description='MSeg datasets management')
    subparsers = parser.add_subparsers(help='')

    # fetch command
    parser_fetch = subparsers.add_parser('fetch', help='Fetch latest datasets available')
    parser_fetch.set_defaults(func=dataset_cmds.fetch)

    # update command
    parser_update = subparsers.add_parser('update', help='Update the local datasets copies')
    parser_update.set_defaults(func=dataset_cmds.update)

    _parse_args(parser)


def mseggen_main():
    parser = argparse.ArgumentParser(prog='mseggen', description='Generate a new motion segmentation algorithm project')
    parser.add_argument('-b', '--base-path', type=str, default=os.getcwd(), help='The new project will be created in BASE_PATH instead of the current working directy if set')
    parser.add_argument('--train', action='store_true', help='Set if the motion segmentation algorithm requires training data')
    parser.add_argument('name', type=str, help='The name of the project. Must be alphanumeric and start with a character')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--cpp', action='store_true', help='C++')
    group.add_argument('--python', action='store_true', help='Python')
    group.add_argument('--java', action='store_true', help='Java')
    group.add_argument('--matlab', action='store_true', help='MATLAB')
    parser.set_defaults(func=generate_cmds.run)

    _parse_args(parser)


def msegdev_main():
    parser = argparse.ArgumentParser(prog='msegdev', description='Tools for MSeg developers')
    dev_subparsers = parser.add_subparsers(help='')
    parser_license = dev_subparsers.add_parser('license', help='List all files within this project that do not have a proper license header')
    parser_license.set_defaults(func=dev_cmds.license)

    _parse_args(parser)


def _parse_args(parser):
    # Register in argcomplete
    argcomplete.autocomplete(parser)
    
    # Run the chosen command
    args = parser.parse_args()
    args.func(args)


class VersionAction(argparse.Action):
    def __init__(self, option_strings, dest, **kwargs):
        super(VersionAction, self).__init__(option_strings, dest, **kwargs)

    def __call__(self, parser, namespace, values=None, option_string=None):
        msegv = self._get_mseg_version()
        msegtv = self._get_mseg_tools_version()

        vers = 'mseg-tools version: {msegtv}\nMSeg version: {msegv}'.format(msegtv=msegtv, msegv=msegv)
        print(vers)
        sys.exit(0)
    
    def _get_mseg_version(self):
        msegv = get_version()
        if msegv is not None:
            mseg_core_dir = os.environ.get('MSEG_CORE_DIR', None)
            appendix = '(deb)'
            if mseg_core_dir is not None:
                appendix = '(compiled from source)'
            return msegv + ' ' + appendix
        else:
            return '?'
    
    def _get_mseg_tools_version(self):
        return v.msegtools_version
