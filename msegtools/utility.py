"""
This file is part of ArmarX.

ArmarX is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

ArmarX is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

@package    MSeg::mseg
@author     Christian R. G. Dreher ( christian.dreher@student.kit.edu )
@date       2018
@copyright  http://www.gnu.org/licenses/gpl-2.0.txt
            GNU General Public License
"""

import os
from ctypes import cdll, c_char_p


DEFAULT_LIB_PATH = '/usr/lib'


def _call_mseg_libutility_so(function_name, default):
    try:
        mseg_core_dir = os.environ.get('MSEG_CORE_DIR', None)
        path = os.path.join(DEFAULT_LIB_PATH, 'libUtility.so')
        if mseg_core_dir is not None:
            path = os.path.join(mseg_core_dir, 'build/lib/libUtility.so')
        utillib = cdll.LoadLibrary(path)
        return c_char_p(utillib[function_name]()).value
    except:
        print 'libUtility.so-function `{}` not available. Using fallback value `{}`'.format(function_name, default)
        return default


def get_version(default=None):
    return _call_mseg_libutility_so('getVersion', default)


def get_mseg_home_path(default=None):
    return _call_mseg_libutility_so('getMSegHomePath', default)


def get_motion_data_path(default=None):
    return _call_mseg_libutility_so('getMotionDataPath', default)
